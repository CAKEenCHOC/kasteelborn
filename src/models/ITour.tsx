import IPage from './IPage'

export default interface ITour {
    id: number
    pages : IPage[]
    preview : string
    title : string
}