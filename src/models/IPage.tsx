import IDialog from "./IDialog";

export default interface IPage {
    description: string
    id: number
    image: string
    video: string
    title: string
    dialogs: IDialog[]
}