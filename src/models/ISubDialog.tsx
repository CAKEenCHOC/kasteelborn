export default interface IDialog {
    description: string
    image: string
    video: string
    title: string
}