import ISubDialog from './ISubDialog';

export default interface IDialog {
    buttonText: string
    description: string
    image: string
    title: string
    subDialogs: ISubDialog[]
}