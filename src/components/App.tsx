import * as React from 'react';
import Home from './Home';
import Tour from './Tour';
import { BrowserRouter as Router, Route } from "react-router-dom";
import backgroundImage from '../backgrounds/background.jpg'


class App extends React.Component {

  public componentWillMount() {
    document.body.style.margin = "0";
    document.body.style.backgroundImage = `linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url(${backgroundImage})`;
    document.body.style.backgroundPosition = 'center';
    document.body.style.backgroundSize = 'cover';
    document.body.style.backgroundRepeat = 'no-repeat';
    document.body.style.backgroundAttachment = 'fixed';
    document.body.style.minHeight = "100vh";
  }

  public render() {
    return (
      <div style={{padding: 16}}>
        <Router>
          <div>
            <Route exact={true} path="/" component={Home} />
            <Route exact={false} path="/tour/:tourid/:pageid" component={Tour} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;