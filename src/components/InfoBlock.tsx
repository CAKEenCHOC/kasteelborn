import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { Typography, Grid } from '@material-ui/core';

interface IInfoBlockProps {
    image: string,
    title: string,
    description: string
}

class InfoBlock extends React.Component<IInfoBlockProps, any> {
    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <Card style={{ marginTop: 32 }}>
                <Grid
                    container
                    direction="column"
                    justify="flex-start"
                    alignItems="center">
                    <CardMedia
                        component="img"
                        style={{maxHeight: 400, maxWidth: 550, paddingTop: 32 }}
                        src={this.props.image} />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2" style={{ textAlign: "center" }}>
                            {this.props.title}
                        </Typography>
                        <Typography component="p" style={{ maxWidth: 800 }}>
                            {this.props.description}
                        </Typography>
                    </CardContent>
                </Grid>
            </Card>
        );

    }
}

export default InfoBlock;