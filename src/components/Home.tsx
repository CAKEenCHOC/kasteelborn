import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import ITour from '../models/ITour';
import { Button, Typography } from '@material-ui/core';
import JsonReader from 'src/util/JsonReader';
import { Link } from "react-router-dom";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';

interface IHomeState {
  tours: ITour[]
}

class Home extends React.Component<any, IHomeState> {
  constructor(props: any) {
    super(props);

    const reader: JsonReader = new JsonReader();
    this.state = {
      tours: reader.importJson(),
    };
  }


  public render() {
    return (
      <div>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center">

          <Grid item>
            <Typography component="h2" variant="h1" gutterBottom style={{ color: "white", textAlign: "center" }}>
              Kasteel Born
            </Typography>
          </Grid>

        </Grid>

        <Grid
          container
          direction="row"
          justify="space-evenly"
          alignItems="center">

          {this.state.tours.map((tour: ITour) => {
            return <Grid item={true} key={tour.title} style={{ marginRight: 75, marginLeft: 75 }}>
              <Card>
                <Link to={"/tour/" + tour.id + "/0"} onClick={() => { window.scrollTo(0, 0) }} style={{ textDecoration: "none" }}>
                  <CardMedia
                    component="img"
                    style={{ maxHeight: 250, maxWidth: 350 }}
                    image={tour.preview}
                  />
                </Link>
              </Card>
              <CardActions style={{ justifyContent: 'center' }}>
                <Typography gutterBottom variant="h5" component="h2" style={{ textAlign: "center" }}>
                  <Link to={"/tour/" + tour.id + "/0"} style={{ textDecoration: "none" }}>
                    <Button size="small" color="primary" onClick={() => { window.scrollTo(0, 0) }} style={{ color: "white" }}>
                      {tour.title}
                    </Button>
                  </Link>
                </Typography>
              </CardActions>
            </Grid>
          })}
        </Grid>
      </div>

    );
  }
}


export default Home;