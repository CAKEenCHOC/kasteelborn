import * as React from 'react';
import { Grid } from '@material-ui/core';
import { Link } from "react-router-dom";
import HomeIcon from '@material-ui/icons/Home';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';

interface INavigationProps {
    previous: boolean,
    next: boolean,
    home: boolean,
    tourid: number,
    pageid: number
}

class Navigation extends React.Component<INavigationProps, any> {
    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={32}>

                {this.props.previous &&
                    <Grid item>
                        <Link to={"/tour/" + this.props.tourid + "/" + (+this.props.pageid - +1)} style={{ textDecoration: "none" }}>
                            <ArrowBack fontSize="large" style={{ color: "white" }} onClick={() => {window.scrollTo(0, 0);}}>Vorige pagina</ArrowBack>
                        </Link>
                    </Grid>

                }
                {this.props.home &&
                    <Grid item>
                        <Link to={"/"} style={{ textDecoration: "none", float: "right" }}>
                            <HomeIcon fontSize="large" style={{ color: "white" }} />
                        </Link>
                    </Grid>
                }
                {this.props.next &&
                    <Grid item>
                        <Link to={"/tour/" + this.props.tourid + "/" + (+this.props.pageid + +1)} style={{ textDecoration: "none" }}>
                            <ArrowForward fontSize="large" style={{ color: "white" }} onClick={() => {window.scrollTo(0, 0);}}>Volgende pagina</ArrowForward>
                        </Link>
                    </Grid>
                }
            </Grid>
        );
    }
}

export default Navigation;