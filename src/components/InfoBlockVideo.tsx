import * as React from 'react';
import { Grid, Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

interface IInfoBlockVideoProps {
    video: string,
    title?: string,
    description?: string,
}

class InfoBlockVideo extends React.Component<IInfoBlockVideoProps, any> {
    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="center">


                {this.props.title && this.props.description ?
                    <Card style={{ marginTop: 32 }}>
                        <CardContent>
                            <div style={{ textAlign: "center" }}>
                                <video key={this.props.video} width="320" height="240" controls>
                                    <source src={this.props.video} type="video/mp4" />
                                    Your browser does not support video playback.
                                </video>
                            </div>
                            <Typography gutterBottom variant="h5" component="h2" style={{ textAlign: "center" }}>
                                {this.props.title}
                            </Typography>
                            <Typography component="p" style={{ maxWidth: 800 }}>
                                {this.props.description}
                            </Typography>
                        </CardContent>
                    </Card>

                    :

                    <video key={this.props.video} width="320" height="240" controls>
                        <source src={this.props.video} type="video/mp4" />
                        Your browser does not support video playback.
                    </video>
                }

            </Grid>
        );

    }
}

export default InfoBlockVideo;