import * as React from 'react';
import JsonReader from 'src/util/JsonReader';
import ITour from '../models/ITour';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import InfoBlock from './InfoBlock';
import Navigation from './Navigation';
import InfoBlockVideo from './InfoBlockVideo';

interface ITourState {
    tours: ITour[],
    loading: boolean,
    tourid: number,
    pageid: number
}

class Tour extends React.Component<any, ITourState> {
    constructor(props: any) {
        super(props);
    }

    public componentDidMount() {
        const reader: JsonReader = new JsonReader();
        this.setState({
            tours: reader.importJson()
        });
    }

    public render() {
        if (!this.state) {
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            );
        }
        const tourid = this.props.match.params.tourid
        const pageid = this.props.match.params.pageid
        return (
            <div style={{ marginBottom: 32 }}>
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center">

                    <Grid item>
                        <Typography variant="h4" gutterBottom style={{ color: "white", textAlign: "center" }}>
                            {this.state.tours[tourid].title}
                        </Typography>
                    </Grid>

                </Grid>

                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center">

                    <Grid item>
                        {this.state.tours[tourid].pages[pageid].image ?
                            <InfoBlock image={this.state.tours[tourid].pages[pageid].image} title={this.state.tours[tourid].pages[pageid].title} description={this.state.tours[tourid].pages[pageid].description} />
                            :
                            <InfoBlockVideo video={this.state.tours[tourid].pages[pageid].video} />
                        }

                    </Grid>

                    {this.state.tours[tourid].pages[pageid].dialogs &&
                        this.state.tours[tourid].pages[pageid].dialogs.map((dialog, i) => {
                            return (
                                dialog.subDialogs.map((subdialog, j) => {
                                    return (
                                        <Grid item key={j}>
                                            {subdialog.image ?
                                                <InfoBlock image={subdialog.image} title={subdialog.title} description={subdialog.description} />
                                            :
                                                <InfoBlockVideo video={subdialog.video} title={subdialog.title} description={subdialog.description} />
                                            }

                                        </Grid>
                                    );
                                })
                            )
                        })
                    }

                    <Grid item style={{ marginTop: 16 }}>
                        <Navigation previous={pageid > 0} home={true} next={this.state.tours[tourid].pages.length - 1 > pageid} tourid={tourid} pageid={pageid} />
                    </Grid>

                </Grid>
            </div>
        );
    }
}

export default Tour;