import * as tours from "../json/tours.json"

class JsonReader {
    public importJson() {
        return (tours as any).tours;
    }
}

export default JsonReader;
